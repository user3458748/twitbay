<!-- Responsible for displaying the summary info for a user -->



<div media="true" class="js-stream-item stream-item">
    <div class="stream-item-content tweet js-actionable-tweet stream-tweet">
        <div class="tweet-image">
            <img src="images/<?= $post->image ?>" alt="Post pic" class="user-profile-link" height="48" width="48">
        </div>
        <div class="tweet-content">
            <a class="tweet-screen-name user-profile-link"><?= $post->name ?></a>
            <p class="tweet-text js-tweet-text"><?= $post->content?></p>
            <p class="tweet-timestamp"><?= $post->date ?></p>
        </div>
    </div>
</div>