<?php

class User
{
  public $email;
  public $name;
  public $id;

  public function __construct ($id, $name, $email)
  {
    $this->id = $id;
    $this->name = $name;
    $this->email = $email;
  }

  public static function all()
  {
    $users = [];

    // connect to the DB
    $mysqli = new mysqli('localhost', 'root', 'root', 'twitbayV2', 3306);
    if (!$mysqli->connect_error)
    {
      // wahoo! We have connected baby! - query all users
      $sql = 'select * from users';
      $mysqliResult = $mysqli->query($sql);
      if ($mysqliResult)
      {
        // loop over all the results
        while($row = $mysqliResult->fetch_assoc())
        {
          // instantiate a user model
          $users[] = new User(
          $row['id'],
          $row['name'],
          $row['email']
          );
        }
      }
    }

    return $users;
  }
}