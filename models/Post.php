<?php

class Post
{
  public $id;
  public $content;
  public $name;
  public $image;
  public $date;
  public $price;



  public function __construct ($id, $content, $name, $image, $date, $price)
  {
    $this->id = $id;
    $this->content = $content;
    $this->name = $name;
    $this->image = $image;
    $this->date = $date;
    $this->price = $price;


  }

  public static function all()
  {
    $posts = [];

    // connect to the DB
    $mysqli = new mysqli('localhost', 'root', 'root', 'twitbayV2', 3306);
    if (!$mysqli->connect_error)
    {
      // wahoo! We have connected baby! - query all users
      $sql = 'SELECT posts.id, posts.content, users.name, users.image, posts.date, posts.price FROM posts INNER JOIN users ON posts.user_id=users.id';
      $mysqliResult = $mysqli->query($sql);
      if ($mysqliResult)
      {
        // loop over all the results
        while($row = $mysqliResult->fetch_assoc())
        {
          // instantiate a user model
          $posts[] = new Post(
          $row['id'],
          $row['content'],
          $row['name'],
          $row['image'],
          $row['date'],
          $row['price']

          );
        }
      }
    }

    return $posts;
  }
}